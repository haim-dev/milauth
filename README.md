# MilaCoins API

Full documentation: [api.milacoins.com](https://api.milacoins.com)

## Sample Usage

```javascript
const apiKey = process.env.MILACOINS_API_KEY;
const secretKey = process.env.MILACOINS_SECRET_KEY;
const url = "https://api.milacoins.com";

const milauth = require("milauth")(url, apiKey, secretKey);
```

## Set the Environment (url)

- SandBox : https://sandbox-api.milacoins.com
- Production: https://api.milacoins.com

## Configure Your Credentials

1. Go to MilaCoins Dashboard( [production](https://milacoins.com), [sandbox](https://sandbox.milacoins.com))
2. Navigate to api settings
3. Generate the api keys
4. add IP to the white list

## Request Method

```javascript
milauth.request(endpoint<String>,{method:<String>[default: 'GET'], query<Object>, body:<Object>})
```

## Request Example

```javascript
try {
  const invoices = await milauth.request("/api/v1/transactions/invoices/", {
    query: { limit: 1 },
  });
  console.log(invoices);
} catch (err) {
  console.log(err);
}
```

OR

```javascript
milauth
  .request("/api/v1/transactions/invoices/", { query: { limit: 1 } })
  .then((res) => console.log(res))
  .catch((err) => console.error(err));
```

## API Errors

Any response status > 399 will throw Error.
**Error properties:**

- requestID : string
- code: number
- message: string
- name: string

Full errors list: [click here](https://api.milacoins.com/#errors)
