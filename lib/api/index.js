const crypto = require("crypto"),
  axios = require("axios"),
  ApiError = require("./error"),
  urls = ["https://sandbox-api.milacoins.com", "https://api.milacoins.com"];

class Api {
  constructor(baseUrl, apiKey, secretKey, opts) {
    this.opts = opts || {};

    this.baseUrl = baseUrl;
    this.apiKey = apiKey;
    this.secretKey = secretKey;

    if (!urls.includes(this.baseUrl)) {
      throw new Error(`baseUrl have to be one of: ${urls.toString()}`);
    }
    if (!this.apiKey) {
      throw new Error("api key is required");
    }

    if (!this.secretKey) {
      throw new Error("secret key is required");
    }
  }

  async request(endPoint, { method = "GET", body, query } = {}) {
    let queryString = "";

    if (query && typeof query !== "object")
      throw new Error("request query have to be Object");
    if (query) {
      queryString = Object.entries(query)
        .map(([key, value], i) => {
          return `${i === 0 ? "?" : ""}${key}=${value}`;
        })
        .join("&");
    }

    const options = {
        url: `${this.baseUrl}${endPoint}${queryString}`,
        method,
        data: body,
        headers: {},
      },
      time = Math.round(Date.now() / 1000),
      hmacSha256 = crypto.createHmac("sha256", this.secretKey);
    hmacSha256.update(
      time + options.method.toUpperCase() + endPoint + queryString
    );
    if (options.data) {
      options.data = JSON.stringify(options.data);
      hmacSha256.update(Buffer.from(options.data, "utf-8"));
    }

    options.headers.Accept = "application/json";
    options.headers["Content-Type"] = "application/json";
    options.headers["X-api-key"] = this.apiKey;
    options.headers["X-time"] = time;
    options.headers["X-Sig"] = hmacSha256.digest("hex");

    try {
      const { data } = await axios(options);
      return data;
    } catch (err) {
      if (err.response && err.response.data)
        throw new ApiError(err.response.data);
      else throw new Error("unknown error");
    }
  }
}

module.exports = Api;
