class MilaCoinsError extends Error {
  constructor({ requestID, message = "unknown error", code, name } = {}) {
    super(message);

    this.requestID = requestID;
    this.code = code;
    this.message = message;
    this.name = name;

    Error.captureStackTrace(this, this.constructor);
  }
}

module.exports = MilaCoinsError;
