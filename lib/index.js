"use strict";

const MilaCoinsApi = require("./api");
//const webhooks = require('./webhooks/webhooks');

const initializer = function (baseUrl, apiKey, secretKey) {
  return new MilaCoinsApi(baseUrl, apiKey, secretKey);
};

module.exports = initializer;
